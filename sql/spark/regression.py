from __future__ import print_function
from pyspark.sql import SparkSession
from pyspark.sql.types import StringType
from pyspark.ml.feature import VectorAssembler
import sys
from pyspark.ml.regression import GeneralizedLinearRegression , LinearRegression , RandomForestRegressor

from pyspark.sql.functions import udf, asc
import json
from datetime import datetime

class Regression:
    def __init__(self, input, output, appName):
        self.input = input
        self.output = output
        self.features = []
        self.models = {}
        self.spark = SparkSession \
            .builder \
            .appName(appName) \
            .getOrCreate()
        self.appName = appName

    def start(self):
        df = self.spark.read.csv(self.input, header=True)
        storeCodes = df.select("storeCode").distinct().collect()
        assembler = VectorAssembler(
                inputCols=["startDate"],
                outputCol="features")

        # df_new = df.select("startDate").rdd.map(lambda x: x[0:2]).toDF().show(truncate=False)
        toInt = udf(lambda s: datetime.strptime(s, '%d/%m/%Y').timetuple()[7], StringType())

        i = 0;
        for s in storeCodes:
            i = i + 1;
            features = df \
                .select(df["startDate"],df["footfalls"]) \
                .filter(df["storeCode"] == s.storeCode) \
                .sort(asc("startDate")) \
                .withColumn("startDate" , toInt(df["startDate"]).cast("int")) \
                .withColumn("footfalls" , df["footfalls"].cast("int"))

            f = assembler.transform(features)
            # f.show()
            self.features.append(f)
            self.train(s,f)
            if i > 0:
                break;
            
        self.spark.stop()

    
    def train(self, code, features):
        # print(features.collect())
        lr = GeneralizedLinearRegression(family="poisson", link="log", labelCol="footfalls")
        model = lr.fit(features)
        transformed = model.transform(features)
        self.models[code.storeCode] = model

        data = {}
        data["features"] = transformed.toJSON(use_unicode=False).collect();

        self.writeTofile(data)

        # # Print the coefficients and intercept for linear regression
        # print("Coefficients: %s" % str(model.coefficients))
        # print("Intercept: %s" % str(model.intercept))

        # # Summarize the model over the training set and print out some metrics
        # trainingSummary = model.summary
        # print("numIterations: %d" % trainingSummary.totalIterations)
        # print("objectiveHistory: %s" % str(trainingSummary.objectiveHistory))
        # trainingSummary.residuals.show()
        # print("RMSE: %f" % trainingSummary.rootMeanSquaredError)
        # print("r2: %f" % trainingSummary.r2)


    def writeTofile(self, data):
        f = open(self.output, 'w')
        f.write(json.dumps(data))
        f.close()


def start(args):
    if(len(args) < 2):
        print("Usage: <foot data> <output>")
        sys.exit(2)
    else:
        regression = Regression(args[1], args[2], "Regression")        
        regression.start()


if __name__ == "__main__":
    start(sys.argv);
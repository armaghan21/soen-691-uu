from __future__ import print_function
from pyspark.sql import SparkSession

from pyspark.sql.types import *
import json

import sys

class BussinesQueries:
    def __init__(self, foot, click, output, appName):
        self.click = click
        self.foot = foot
        self.output = output
        self.spark = SparkSession \
            .builder \
            .appName(appName) \
            .getOrCreate()
        self.appName = appName
        self.footDf = self.spark.read.csv(self.foot , header=True)
        self.footDf = self.footDf.withColumn("footfalls" , self.footDf["footfalls"].cast("double"))
        self.clickDf = self.spark.read.csv(self.click , header=True)
        self.clickDf.createOrReplaceTempView("click")
        self.footDf.createOrReplaceTempView("foot")

    
    
    def writeTofile(self,data):
        f = open(self.output, 'w')
        f.write(json.dumps(data))
        f.close()

    def queriyBuilder(self,query):
        res = 0
        if(query == "popularstore"):
            res = self.popularStore()
        elif True:
            pass
        self.writeTofile(res)   

    def popularStore(self):
        res = self.footDf.select("storeCode" , "footfalls").sort("footfalls", ascending=False).toJSON(use_unicode=False)
        dic = {}
        dic["data"] = res.collect()
        return dic
    
    def stop(self):
        self.spark.stop()


def start(args):
    if(len(args) < 6):
        print("Usage: <foot data> <click data> <output> -q <query name>")
        sys.exit(2)
    else:
        bussinesQueries = BussinesQueries(args[1], args[2], args[3], "bussines query")        
        bussinesQueries.queriyBuilder(args[5])
        bussinesQueries.stop()

if __name__ == "__main__":
    start(sys.argv)
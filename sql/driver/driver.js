const SCRIPTS_DIRECTORY = __base + "sql/spark/";
const SPARK_SUBMIT = "spark-submit";
const spawn = require('child_process').spawn;
const fs = require('fs');


function regression(callback) {
    const stream = fs.createReadStream(SCRIPTS_DIRECTORY + 'regoutput.out');
    stream.on('error', (err) => {
        console.log(err);
        
        const ls = spawn(SPARK_SUBMIT, [SCRIPTS_DIRECTORY + "regression.py", 
        SCRIPTS_DIRECTORY + 'input/footfall.csv',
        SCRIPTS_DIRECTORY + 'regoutput.out']);

        ls.stderr.on('data', function (chunk) {
            console.log(chunk.toString());
        });

        ls.on('error', (err) => {
            console.log('Failed to start child process.');
            console.log(err);
        });

        ls.on('close', (code) => {
            console.log(`child process exited with code ${code}`);
            const rr = fs.createReadStream(SCRIPTS_DIRECTORY + 'regoutput.out');
            callback(rr);
            
            rr.on('end', () => {
                console.log('There will be no more data.');
            });
        });
    });

    stream.on('open', () => {
        console.log('file exist');
        callback(stream);
    });

    stream.on('end', () => {
        console.log('There will be no more data.');
    });
}

function queries(query, callback) {
    const stream = fs.createReadStream(SCRIPTS_DIRECTORY + 'output.out');
    stream.on('error', (err) => {
        console.log(err);
        const ls = spawn(SPARK_SUBMIT, [SCRIPTS_DIRECTORY + "queries.py", 
            SCRIPTS_DIRECTORY + 'input/footsummary.csv',
            SCRIPTS_DIRECTORY + 'input/clickdata.csv',
            SCRIPTS_DIRECTORY + 'output.out',
            "-q",
            query]);

        ls.stderr.on('data', function (chunk) {
            console.log(chunk.toString());
        });

        ls.on('error', (err) => {
            console.log('Failed to start child process.');
            console.log(err);
        });

        ls.on('close', (code) => {
            console.log(`child process exited with code ${code}`);
            const rr = fs.createReadStream(SCRIPTS_DIRECTORY + 'output.out');
            callback(rr);
            
            rr.on('end', () => {
                console.log('There will be no more data.');
            });
        });
    });

    stream.on('open', () => {
        console.log('file exist');
        callback(stream);
    });

    stream.on('end', () => {
        console.log('There will be no more data.');
    });

    // const ls = spawn(SPARK_SUBMIT, [SCRIPTS_DIRECTORY + "queries.py", 
    //         SCRIPTS_DIRECTORY + 'input/footsummary.csv',
    //         SCRIPTS_DIRECTORY + 'input/clickdata.csv',
    //         SCRIPTS_DIRECTORY + 'output.out',
    //         "-q",
    //         query]
    //         );
    // ls.stderr.on('data', function (chunk) {
    //     console.log("error : " + chunk.toString());
    // });
    // ls.on('error', (err) => {
    //     console.log('Failed to start child process.');
    //     console.log(err);
    // });

    // ls.on('close', (code) => {
    //     console.log(`child process exited with code ${code}`);
    //     const rr = fs.createReadStream(SCRIPTS_DIRECTORY + 'output.out');
    //     callback(rr);
        
    //     rr.on('end', () => {
    //         console.log('There will be no more data.');
    //     });
    // });
}

exports.queries = queries;
exports.regression = regression;

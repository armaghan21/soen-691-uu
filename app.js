var express = require("express");
var bodyParser = require("body-parser");
var app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

global.__base = __dirname + '/';

app.use('/js', express.static(__dirname + '/node_modules/bootstrap/dist/js')); 
app.use('/js', express.static(__dirname + '/node_modules/jquery/dist'));
app.use('/js', express.static(__dirname + '/node_modules/chart.js/dist'));
app.use('/js', express.static(__dirname + '/clientJs'));
app.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css'));

var router = express.Router();
 
var routes = require(__base + "routes/routes.js")(router);

app.get('/',function(req,res){
  res.sendFile(__base+'index.html');
});

app.use('/api', router);

var server = app.listen(3000, function () {
    console.log("Listening on port %s...", server.address().port);
});
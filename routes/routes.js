var driver = require(__base + "sql/driver/driver.js");

var appRouter = function(router) {

    router.get("/regression", function(req, res) {
        // res.send("hello");
        driver.regression((stream) => {
            stream.pipe(res);
        });
    });

    router.get("/popularstore", function(req, res) {
        // res.send("Heloo");
        driver.queries("popularstore", (stream) => {
            stream.pipe(res);
        });
    });
}
 
module.exports = appRouter;
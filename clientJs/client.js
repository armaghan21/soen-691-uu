var q = $("#query");
var r = $("#regression");
var s = $("#prediction");

var result = $.ajax({url: "http://localhost:3000/api/popularstore"}).done(function(data) {
    let list = JSON.parse(data).data;

    let labels = [];
    let d = [];

    for (let n of list) {
        let o = JSON.parse(n);
        labels.push(o.storeCode);
        d.push(o.footfalls);
    }

    creatChart(labels, d, q, 'bar', 'store names');
    
});

var reg = $.ajax({url: "http://localhost:3000/api/regression"}).done(function(data) {
    let list = JSON.parse(data).features;

    let labels = [];
    let d = [];
    let p = [];

    for (let n of list) {
        let o = JSON.parse(n);
        labels.push(o.startDate);
        d.push(o.footfalls);
        p.push(o.prediction);
    }

    creatChart(labels, d, r, 'line', 'footfalls');

    creatChart(labels, p, s, 'line', 'predictions');    


});

function creatChart(labels , data, node, type, name) {
    var myChart = new Chart(node, {
        type: type,
        data: {
            labels: labels,
            datasets: [{
                label: name,
                data: data,
                // backgroundColor: [
                //     'rgba(255, 99, 132, 0.2)',
                //     'rgba(54, 162, 235, 0.2)',
                //     'rgba(255, 206, 86, 0.2)',
                //     'rgba(75, 192, 192, 0.2)',
                //     'rgba(153, 102, 255, 0.2)',
                //     'rgba(255, 159, 64, 0.2)'
                // ],
                // borderColor: [
                //     'rgba(255,99,132,1)',
                //     'rgba(54, 162, 235, 1)',
                //     'rgba(255, 206, 86, 1)',
                //     'rgba(75, 192, 192, 1)',
                //     'rgba(153, 102, 255, 1)',
                //     'rgba(255, 159, 64, 1)'
                // ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
}